# Recruitment-task

Application to asynchronous processing web resources.
It uses MySQL database to store urls requested by client.
Also exposes API to:

* fetch all web resources URLs which are stored
* fetch web resource content specified by location
* find web resource which contains specified sequence 

## Running
```
mvn clean package
docker-compose build
docker-compose up
```
## API docs

Swagger docs is available on: [http://localhost:8080/swagger-ui.html#/](http://localhost:8080/swagger-ui.html#/)

## Tests

There are integration tests which use embedded MySQL and embedded RabbitMq.

Before running tests execute command:

``apt install libncurses5`` - required by embedded mysql

``apt install erl`` - required by embedded rabbitmq

Note: If RabbitMQ fails to start with a message 
```{"init terminating in do_boot",{undef,[{rabbit_prelaunch,start,[]},{init,start_it,1},{init,start_em,1}]}}```
or ```ErlangVersionException: Could not determine Erlang version. Ensure Erlang is correctly installed```
check Erlang compatibility with RabbitMQ [https://www.rabbitmq.com/which-erlang.html](https://www.rabbitmq.com/which-erlang.html)

After running ``mvn clean package`` coverage report is available on: ``target/site/jacoco/index.html``
Test execution report is also available after running command: ``xdg-open target/site/surefire-report.html``

## Diagrams

* Asynchronous fetching web resource
 
![Diagram 1](src/main/resources/diagram1.jpg)


* Getting web resource content

![Diagram 2](src/main/resources/diagram2.jpg)

## Testing

* Fetching web resource
```
curl -i  -XPOST -H "Content-Type: application/json" http://localhost:8080/api/v1/resources -d '{"location": "https://www.google.com/"}'
HTTP/1.1 200 
Content-Length: 0
Date: Sun, 15 Nov 2020 17:28:40 GMT
```

* Fetching all web resources
```
curl http://localhost:8080/api/v1/resources 
```

* Fetching web resource content by location
```
curl http://localhost:8080/api/v1/resources/content?location=https://www.google.com/
```

* Fetching web resources by sequence
```
curl http://localhost:8080/api/v1/resources/sequence?sequence=google
```