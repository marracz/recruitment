package com.bluemedia.recruitment.rest;

import com.bluemedia.recruitment.service.WebResourceService;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {WebResourceController.class})
@WebMvcTest
class WebResourceControllerTest {

    @MockBean
    private WebResourceService webResourceService;

    @Autowired
    private MockMvc mockMvc;

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"ftp://blumedia.com", "cccccckevnfhglievurlhvbnklifgtuuehhicvvlhtlh"})
    public void shouldReturnBadRequest(String location) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/resources")
                .content(String.format("{\"location\": \"%s\"}", location))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @ParameterizedTest
    @ValueSource(strings = {"https://bluemedia.pl/", "http://bluemedia.pl/"})
    public void shouldReturnOk(String location) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/resources")
                .content(String.format("{\"location\": \"%s\"}", location))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

}