package com.bluemedia.recruitment.rest;

import com.bluemedia.recruitment.model.WebResourceEntity;
import com.bluemedia.recruitment.persistence.WebResourceRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wix.mysql.config.MysqldConfig;
import io.arivera.oss.embedded.rabbitmq.EmbeddedRabbitMq;
import io.arivera.oss.embedded.rabbitmq.EmbeddedRabbitMqConfig;
import io.arivera.oss.embedded.rabbitmq.PredefinedVersion;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.wix.mysql.EmbeddedMysql.anEmbeddedMysql;
import static com.wix.mysql.config.MysqldConfig.aMysqldConfig;
import static com.wix.mysql.distribution.Version.v5_7_27;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(properties = {
        "spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3307/bluemedia_it",
        "spring.datasource.username=user_it",
        "spring.datasource.password=pass_it",
        "spring.rabbitmq.port=5673"
})
public class ApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebResourceRepository resourceRepository;

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @BeforeAll
    static void beforeAll() {
        startMysql();
        startRabbitMq();
    }

    @Test
    public void shouldReturnAllRecords() throws Exception {
        // given
        resourceRepository.save(webResource("https://www.bluemedia.pl", "abc"));
        resourceRepository.save(webResource("http://www.wp.pl", "def"));
        resourceRepository.save(webResource("https://www.onet.pl", "ghi"));

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/resources"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        List<WebResourceMetadata> resources = MAPPER.readValue(mvcResult.getResponse().getContentAsString(),
                new TypeReference<>() {});
        assertThat(resources).hasSize(3);
        List<String> resourcesUrls = resources.stream().map(WebResourceMetadata::getLocation).collect(Collectors.toList());
        assertThat(resourcesUrls).containsExactly(
                "http://www.wp.pl", "https://www.bluemedia.pl", "https://www.onet.pl");
    }

    @Test
    public void shouldReturnRecordMatchesUrl() throws Exception {
        // given
        resourceRepository.save(webResource("https://www.bluemedia.pl", "abc"));
        resourceRepository.save(webResource("http://www.wp.pl", "def"));
        resourceRepository.save(webResource("https://www.onet.pl", "ghi"));

        // when
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/resources/content?location=https://www.bluemedia.pl"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        assertThat(mvcResult.getResponse().getContentAsString()).isEqualTo("abc");
    }

    @Test
    public void shouldThrowNotFoundException() throws Exception {
        // given
        resourceRepository.save(webResource("https://www.bluemedia.pl", "abc"));
        resourceRepository.save(webResource("http://www.wp.pl", "def"));
        resourceRepository.save(webResource("https://www.onet.pl", "ghi"));

        // when
        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/resources/content?location=https://www.interia.pl"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void shouldReturnWebResourceWithContainsSequence() throws Exception {
        // given
        resourceRepository.save(webResource("https://www.bluemedia.pl", "abc"));
        resourceRepository.save(webResource("http://www.wp.pl", "<html>4300 zachorowań na covid-19</html>"));
        resourceRepository.save(webResource("https://www.onet.pl", "ghi"));

        // when
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/resources/sequence?sequence=covid"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        List<WebResourceMetadata> resources = MAPPER.readValue(mvcResult.getResponse().getContentAsString(),
                new TypeReference<>() {});
        assertThat(resources).hasSize(1);
        List<String> resourcesUrls = resources.stream().map(WebResourceMetadata::getLocation).collect(Collectors.toList());
        assertThat(resourcesUrls).containsExactly("http://www.wp.pl");
    }

    @Test
    public void shouldAddToQueueAndSaveInDatabase() throws Exception {

        // when
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/resources")
                        .content(String.format("{\"location\": \"%s\"}", "https://www.github.com"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        Thread.sleep(3000);

        // then
        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/resources"))
                .andExpect(status().isOk())
                .andReturn();

        List<WebResourceMetadata> resources = MAPPER.readValue(mvcResult.getResponse().getContentAsString(),
                new TypeReference<>() {});
        List<String> resourcesUrls = resources.stream().map(WebResourceMetadata::getLocation).collect(Collectors.toList());
        assertThat(resourcesUrls).contains("https://www.github.com");

    }

    private WebResourceEntity webResource(String location, String content) {
        return new WebResourceEntity(UUID.randomUUID(), location, content.getBytes(), 10,
                "text/html", LocalDateTime.now());
    }

    private static void startMysql() {
        MysqldConfig config = aMysqldConfig(v5_7_27)
                .withPort(3307)
                .withUser("user_it", "pass_it")
                .build();

        anEmbeddedMysql(config)
                .addSchema("bluemedia_it")
                .start();
    }

    private static void startRabbitMq() {
        EmbeddedRabbitMqConfig config = new EmbeddedRabbitMqConfig.Builder()
                .version(PredefinedVersion.V3_8_0)
                .port(5673)
                .rabbitMqServerInitializationTimeoutInMillis(10000)
                .extractionFolder(new File("/tmp/rabbit"))
                .build();
        EmbeddedRabbitMq rabbitMq = new EmbeddedRabbitMq(config);
        rabbitMq.start();
    }
}
