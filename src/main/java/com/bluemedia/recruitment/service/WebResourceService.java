package com.bluemedia.recruitment.service;

import com.bluemedia.recruitment.model.WebResourceEntity;
import com.bluemedia.recruitment.persistence.WebResourceRepository;
import com.bluemedia.recruitment.rest.WebResourceMetadata;
import com.bluemedia.recruitment.rest.WebResourceNotFound;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.bluemedia.recruitment.config.QueueConfiguration.QUEUE_NAME;

/**
 * Service which delegates user requests to proper component
 */
@Slf4j
@Service
public class WebResourceService {

    private final WebResourceRepository resourceRepository;
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public WebResourceService(WebResourceRepository resourceRepository,
                              RabbitTemplate rabbitTemplate) {
        this.resourceRepository = resourceRepository;
        this.rabbitTemplate = rabbitTemplate;
    }

    public void fetchResource(String location) {
        log.debug("Adding resource {} to queue", location);
        rabbitTemplate.convertAndSend(QUEUE_NAME, location);
    }

    public Collection<WebResourceMetadata> fetchAll(int page, int size) {
        return resourceRepository.findAll(PageRequest.of(page, size, Sort.by("location"))).stream()
                .map(x -> new WebResourceMetadata(x.getId(), x.getLocation()))
                .collect(Collectors.toList());
    }

    public WebResourceEntity fetchResourceByLocation(String location) {
        Optional<WebResourceEntity> resourceContentByLocation = resourceRepository.findFirstByLocation(location);
        return resourceContentByLocation
                .orElseThrow(() ->
                        new WebResourceNotFound(String.format("Web resource with location %s not found", location)));
    }

    public List<WebResourceMetadata> fetchResourceBySequence(String sequence) {
        String query = "%".concat(sequence).concat("%");
        return resourceRepository.findBySequence(query.getBytes());
    }
}
