package com.bluemedia.recruitment.service;

import com.bluemedia.recruitment.model.WebResourceEntity;
import com.bluemedia.recruitment.persistence.WebResourceRepository;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Component for fetching web resource content using http client
 */
@Slf4j
@Service
public class MessageProcessor {

    private final WebResourceRepository resourceRepository;
    private final OkHttpClient okHttpClient;

    @Autowired
    public MessageProcessor(WebResourceRepository resourceRepository,
                            OkHttpClient okHttpClient) {
        this.resourceRepository = resourceRepository;
        this.okHttpClient = okHttpClient;
    }

    public void process(String location) throws IOException {
        Request request = new Request.Builder()
                .url(location)
                .build();
        Response response = okHttpClient.newCall(request).execute();
        WebResourceEntity entity = createWebResourceEntity(location, response);
        resourceRepository.save(entity);
        log.debug("Url {} saved", location);
    }

    private WebResourceEntity createWebResourceEntity(String location, Response response) throws IOException {
        Headers headers = response.headers();
        byte[] bytes = response.body().bytes();
        return new WebResourceEntity(UUID.randomUUID(), location, bytes,
                bytes.length, headers.get(HttpHeaders.CONTENT_TYPE), LocalDateTime.now());
    }
}
