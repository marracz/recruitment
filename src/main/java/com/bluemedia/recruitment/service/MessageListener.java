package com.bluemedia.recruitment.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.bluemedia.recruitment.config.QueueConfiguration.DLQ_QUEUE_NAME;
import static com.bluemedia.recruitment.config.QueueConfiguration.QUEUE_NAME;

/**
 * Listener for fetching and retrying messages
 */
@Slf4j
@Component
public class MessageListener {

    private final MessageProcessor messageProcessor;

    @Autowired
    public MessageListener(MessageProcessor messageProcessor) {
        this.messageProcessor = messageProcessor;
    }

    @RabbitListener(
            queues = QUEUE_NAME,
            concurrency = "2",
            ackMode = "AUTO",
            containerFactory = "listenerFactory")
    public void fetch(Message message) throws IOException {
            String location = new String(message.getBody());
            log.debug("Polling url from queue: {}", location);
            messageProcessor.process(location);
    }

    @RabbitListener(queues = DLQ_QUEUE_NAME, concurrency = "1", ackMode = "AUTO")
    public void listenDlq(Message message) {
        log.error("Url {} fetch max attempts exceeded", new String(message.getBody()));
    }

}