package com.bluemedia.recruitment.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Entity to store web resource data in database
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "web_resource")
@Entity
public class WebResourceEntity {

    @Id
    private UUID id;
    @Column(name = "location", nullable = false)
    private String location;
    @Lob
    @Column(name = "content", columnDefinition = "MEDIUMBLOB", nullable = false)
    private byte[] content;
    @Column(name = "size")
    private int size;
    @Column(name = "content_type")
    private String contentType;
    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;
}
