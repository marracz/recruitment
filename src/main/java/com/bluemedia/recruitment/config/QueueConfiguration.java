package com.bluemedia.recruitment.config;

import org.aopalliance.aop.Advice;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;

import java.util.HashMap;
import java.util.Map;

/**
 * Configures queue
 */
@Configuration
public class QueueConfiguration {

    public static final String QUEUE_NAME = "bluemedia-queue";
    public static final String DLQ_QUEUE_NAME = "bluemedia-queue-dlq";
    private static final String X_MESSAGE_TTL = "x-message-ttl";
    private static final int MESSAGE_TTL = 3600000;
    private static final String EXCHANGE_NAME = "dlq-exchange";
    private static final String DLQ_EXCHANGE_KEY = "x-dead-letter-exchange";
    private final ConnectionFactory connectionFactory;

    @Autowired
    public QueueConfiguration(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Bean
    public Queue blueMediaQueue() {
        Map<String, Object> args = new HashMap<>();
        args.put(DLQ_EXCHANGE_KEY, EXCHANGE_NAME);
        args.put(X_MESSAGE_TTL, MESSAGE_TTL);
        return new Queue(QUEUE_NAME, false, false, false, args);
    }

    @Bean
    public SimpleRabbitListenerContainerFactory listenerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setConcurrentConsumers(3);
        factory.setMaxConcurrentConsumers(10);
        factory.setAdviceChain(new Advice[] {retries()});
        return factory;
    }

    @Bean
    public Queue crawlerQueueDlq() {
        return new Queue(DLQ_QUEUE_NAME, false);
    }

    @Bean
    public FanoutExchange exchange() {
        return new FanoutExchange(EXCHANGE_NAME);
    }

    @Bean
    public Binding binding(Queue crawlerQueueDlq, FanoutExchange exchange) {
        return BindingBuilder.bind(crawlerQueueDlq).to(exchange);
    }

    @Bean
    public RetryOperationsInterceptor retries() {
        return RetryInterceptorBuilder.stateless()
                .maxAttempts(5)
                .backOffOptions(10000, 2.0, 60_000)
                .recoverer(new RejectAndDontRequeueRecoverer())
                .build();
    }
}
