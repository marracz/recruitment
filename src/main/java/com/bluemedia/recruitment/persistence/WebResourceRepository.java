package com.bluemedia.recruitment.persistence;

import com.bluemedia.recruitment.model.WebResourceEntity;
import com.bluemedia.recruitment.rest.WebResourceMetadata;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Database layer, executes queries
 */
public interface WebResourceRepository extends PagingAndSortingRepository<WebResourceEntity, UUID> {

    Optional<WebResourceEntity> findFirstByLocation(@Param("location") String location);

    @Query("select new com.bluemedia.recruitment.rest.WebResourceMetadata(resource.id, resource.location) " +
            "FROM WebResourceEntity resource WHERE resource.content LIKE :sequence")
    List<WebResourceMetadata> findBySequence(@Param("sequence") byte[] sequence);
}
