package com.bluemedia.recruitment.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;

/**
 * Represents web resource fetch request body
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WebResourceFetchRequest {

    @NotBlank
    @URL(regexp = "^(http|https).*")
    private String location;
}
