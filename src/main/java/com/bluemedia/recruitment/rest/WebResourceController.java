package com.bluemedia.recruitment.rest;

import com.bluemedia.recruitment.model.WebResourceEntity;
import com.bluemedia.recruitment.service.WebResourceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

/**
 * Handles http rest requests
 */
@RestController
public class WebResourceController {

    private final WebResourceService webResourceService;

    @Autowired
    public WebResourceController(WebResourceService webResourceService) {
        this.webResourceService = webResourceService;
    }

    @ApiOperation(value = "Add web resource for queue, which save it asynchronous in storage")
    @PostMapping(value = "/api/v1/resources", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> fetchResource(@Valid @RequestBody WebResourceFetchRequest request) {
        webResourceService.fetchResource(request.getLocation());
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Get all web resources metadata")
    @GetMapping(value = "/api/v1/resources", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<WebResourceMetadata>> fetchAll(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        return ResponseEntity.ok(webResourceService.fetchAll(page, size));
    }

    @ApiOperation(value = "Get content for specified web resource")
    @GetMapping(value = "/api/v1/resources/content", produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<byte[]> fetchResourceByLocation(@RequestParam("location") String location) {
        WebResourceEntity webResourceEntity = webResourceService.fetchResourceByLocation(location);
        String contentType = webResourceEntity.getContentType();
        byte[] body = webResourceEntity.getContent();
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType)).body(body);
    }

    @ApiOperation(value = "Get web resource metadata for resource which contains specified sequence")
    @GetMapping(value = "/api/v1/resources/sequence", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<WebResourceMetadata>> fetchBySequence(@RequestParam("sequence") String sequence) {
        return ResponseEntity.ok(webResourceService.fetchResourceBySequence(sequence));
    }
}
