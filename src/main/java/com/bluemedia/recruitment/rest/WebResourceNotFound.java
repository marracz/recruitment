package com.bluemedia.recruitment.rest;

/**
 * Exception throws when web resource was not found
 */
public class WebResourceNotFound extends RuntimeException {

    public WebResourceNotFound(String message) {
        super(message);
    }
}
