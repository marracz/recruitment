package com.bluemedia.recruitment.rest;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

/**
 * Metadata of web resource used when returning list
 */
@AllArgsConstructor
@Data
public class WebResourceMetadata {

    private final UUID id;
    private final String location;
}
