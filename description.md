#  Zadanie praktyczne.

Problem:

W pewnej firmie istnieje potrzeba przechowywania niektórych zasobów internetowych (stron, plików, itp.) w bazie danych.

 

Założenia:

* System udostępnia usługę (REST lub SOAP) umożliwiająca przekazanie URL strony do pobrania
* Nie trzeba podążać za linkami zawartymi na podanej stronie
* Pobieranie stron odbywa się asynchronicznie w stosunku do przyjmowanych żądań
* Pobieranie zasobu rozpoczyna się od momentu przyjęcia żądania (Chyba, że inne zasoby oczekują na pobranie)

 

To co nie zostało wyspecyfikowanie można przyjąć wg własnego uznania jednak należy zaznaczyć, że przyjmuje się takie założenie.

 

Zadanie:

a) Zaimplementuj aplikację wg powyższych założeń w technologii JEE lub Spring:

* Usługa REST lub SOAP – punkt wejścia do aplikacji
* pobieranie zasobów, organizacja kolejki – warstwa logiki
* zapisywanie zasobów – baza danych
 

b*) Zaimplementuj pobranie listy zasobów z bazy (np. Lista URL)  i pobranie zawartości pojedynczego zasobu

c*) Zaimplementuj wyszukiwanie zasobów, które posiadają zadany ciąg znaków

 

Ocena zadnia:

Kryteria oceny obejmują (w kolejności od najważniejszego):

* poprawność działania
* trafność wykorzystania technologii JEE/Spring
* prostota rozwiązania
* wykorzystanie bibliotek open source'owych
* ew. usprawnienia i dodatkowa funkcjonalność
* styl programowania